[[!meta copyright="Copyright © 2007, 2008, 2024 Free Software
Foundation, Inc."]]

[[!meta license="""[[!toggle id="license" text="GFDL 1.2+"]][[!toggleable
id="license" text="Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no Invariant
Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
is included in the section entitled
[[GNU Free Documentation License|/fdl]]."]]"""]]

<!-- http://richtlijn.be/~larstiq/hurd/hurd-2010-08-25 -->

`storeio` is a translator for devices and other stores.  You can use
it for user-level access to disks via `/dev/hd0s1` instead of kernel-based
device access.

	$ settrans -ca foo /hurd/storeio myfile

Now, foo will look like a device, which gives you transparent
decompression, partition handling, etc.  It is a little like Linux's
`losetup`, and you don't have to be root to use it!

It relies heavily on [[libstore]].


# Examples

You can make a file's content available as some block device (where `foo` is
the name of the file to map):

    settrans -ca node /hurd/storeio -T file foo

You can even `ungzip` files on the fly (`bunzip2` is available as well):

    settrans -ca node /hurd/storeio -T gunzip foo.gz

You can use the *typed store*, to create filter chains (of course this example
is kind of useless since you could use the `gunzip` store directly):

    settrans -ca node /hurd/storeio -T typed gunzip:file:foo.gz
